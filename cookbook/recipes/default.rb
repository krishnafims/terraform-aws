#
# Cookbook:: testcookbook
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

file '/tmp/index.txt' do
  content '<html>This is a placeholder for the home page.</html>'
  mode '0755'
end
